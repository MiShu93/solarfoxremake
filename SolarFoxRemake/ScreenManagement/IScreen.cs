﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolarFoxRemake.ScreenManagement
{
    public interface IScreen
    {
        void LoadContent();
        void UnloadContent();
        void HandleInput();
        void Update(double dt);
        void Draw();
    }
}