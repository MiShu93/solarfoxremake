﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.ViewportAdapters;
using SolarFoxRemake.Factories;
using SolarFoxRemake.Input;
using System;
using System.Collections.Generic;

namespace SolarFoxRemake.ScreenManagement
{
    public class ScreenManager : DrawableGameComponent
    {
        private ScreenFactory screenFactory;
        private List<Screen> screens = new List<Screen>();
        private List<PendingChange> pendingChanges = new List<PendingChange>();

        public BoxingViewportAdapter DefaultViewport { get; private set; }
        public Vector2 DisplayCenter { get; private set; }

        public InputState Input { get; private set; }
        public ContentManager ContentManager { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }

        public ScreenManager(Game game) : base(game)
        {
            Input = new InputState();
            screenFactory = Game.Services.GetService<ScreenFactory>();
            ContentManager = Game.Content;
        }

        public void AddRequest(Type screenType)
        {
            pendingChanges.Add(new PendingChange(Change.Add, screenType));
        }

        public void RemoveRequest(Type screenType)
        {
            pendingChanges.Add(new PendingChange(Change.Remove, screenType));
        }

        private void CommitChanges()
        {
            foreach (var pendingChange in pendingChanges)
            {
                switch (pendingChange.Change)
                {
                    case Change.Add:
                        var screen = screenFactory.CreateScreen(this, pendingChange.ScreenType);
                        screen.LoadContent();
                        screens.Add(screen);
                        break;
                    case Change.Remove:
                        screens.RemoveAll(x => x.GetType() == pendingChange.ScreenType);
                        break;
                }
            }

            pendingChanges.Clear();

            if (screens.Count == 0)
            {
                Game.Exit();
            }
        }

        public override void Initialize()
        {
            DisplayCenter = new Vector2(SolarFoxRemakeGame.VIRTUAL_WIDTH / 2f, SolarFoxRemakeGame.VIRTUAL_HEIGHT / 2f);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            DefaultViewport = new BoxingViewportAdapter(Game.Window, Game.GraphicsDevice, SolarFoxRemakeGame.VIRTUAL_WIDTH, SolarFoxRemakeGame.VIRTUAL_HEIGHT);
            SpriteBatch = new SpriteBatch(Game.GraphicsDevice);

            CommitChanges();
        }

        protected override void UnloadContent()
        {
            foreach (var screen in screens)
            {
                screen.UnloadContent();
            }
        }

        public override void Update(GameTime gameTime)
        {
            Input.Update();
            foreach (var screen in screens)
            {
                screen.HandleInput();
            }

            CommitChanges();

            double deltaTime = gameTime.ElapsedGameTime.TotalSeconds;
            foreach (var screen in screens)
            {
                screen.Update(deltaTime);
            }

            CommitChanges();
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (var screen in screens)
            {
                screen.Draw();
            }
        }
    }
}