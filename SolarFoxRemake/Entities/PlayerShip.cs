﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using SolarFoxRemake.Engine;
using SolarFoxRemake.Input;
using System;

namespace SolarFoxRemake.Entities
{
    public enum PlayerAction
    {
        None,
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight
    }

    public class PlayerShip : Entity
    {
        private PlayerAction action = PlayerAction.None;

        public PlayerShip(Board board, Texture2D texture, Vector2 position) : base(board, texture, position)
        {
            Velocity = new Vector2(4);
        }

        public void HandleInput(InputState input)
        {
            // Keyboard input
            if (input.IsKeyPressed(Keys.Up))
                action = PlayerAction.MoveUp;
            if (input.IsKeyPressed(Keys.Down))
                action = PlayerAction.MoveDown;
            if (input.IsKeyPressed(Keys.Left))
                action = PlayerAction.MoveLeft;
            if (input.IsKeyPressed(Keys.Right))
                action = PlayerAction.MoveRight;

            // Mouse input
            if (input.IsMouseButtonPressed(MouseButton.Left))
            {
                var mousePosition = input.GetMousePosistion();
                var deltaPosition = mousePosition - Position;

                if (Math.Abs(deltaPosition.X) > Math.Abs(deltaPosition.Y))
                {
                    if (deltaPosition.X > 0)
                        action = PlayerAction.MoveRight;
                    if (deltaPosition.X < 0)
                        action = PlayerAction.MoveLeft;
                }
                else
                {
                    if (deltaPosition.Y > 0)
                        action = PlayerAction.MoveDown;
                    if (deltaPosition.Y < 0)
                        action = PlayerAction.MoveUp;
                }
            }

            // Touch input
            if (input.IsGestureAvailable(GestureType.Tap))
            {
                var tapPosistion = input.GetFirstGesture(GestureType.Tap).Position;
                var deltaPosition = tapPosistion - Position;

                if (Math.Abs(deltaPosition.X) > Math.Abs(deltaPosition.Y))
                {
                    if (deltaPosition.X > 0)
                        action = PlayerAction.MoveRight;
                    if (deltaPosition.X < 0)
                        action = PlayerAction.MoveLeft;
                }
                else
                {
                    if (deltaPosition.Y > 0)
                        action = PlayerAction.MoveDown;
                    if (deltaPosition.Y < 0)
                        action = PlayerAction.MoveUp;
                }
            }
        }

        public override void Update(double dt)
        {
            if (board.Contains(Position))
            {
                switch (action)
                {
                    case PlayerAction.MoveUp:
                        Direction = Directions.Up;
                        Rotation = 0;
                        break;

                    case PlayerAction.MoveDown:
                        Direction = Directions.Down;
                        Rotation = MathHelper.ToRadians(180);
                        break;

                    case PlayerAction.MoveLeft:
                        Direction = Directions.Left;
                        Rotation = MathHelper.ToRadians(270);
                        break;

                    case PlayerAction.MoveRight:
                        Direction = Directions.Right;
                        Rotation = MathHelper.ToRadians(90);
                        break;
                }

                action = PlayerAction.None;
            }

            base.Update(dt);

            Position = new Vector2
            {
                X = MathHelper.Clamp(Position.X, board.Centers[1, 1].X, board.Centers[1, Board.COLS - 2].X),
                Y = MathHelper.Clamp(Position.Y, board.Centers[1, 1].Y, board.Centers[Board.ROWS - 2, 1].Y)
            };
        }
    }
}