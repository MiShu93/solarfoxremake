﻿using System;

namespace SolarFoxRemake.ScreenManagement
{
    public enum Change
    {
        Add,
        Remove
    }

    public class PendingChange
    {
        public Change Change { get; private set; }
        public Type ScreenType { get; private set; }

        public PendingChange(Change change, Type screenType)
        {
            Change = change;
            ScreenType = screenType;
        }
    }
}