using Android.Content.Res;
using SolarFoxRemake.Engine;
using System.IO;

namespace SolarFoxRemake.Droid.Platform
{
    public class AndroidLevelLoader : LevelLoader
    {
        private AssetManager assets;

        public AndroidLevelLoader(AssetManager assets)
        {
            this.assets = assets;
        }

        public override Level Load()
        {
            using (StreamReader sr = new StreamReader(assets.Open(Filename)))
            {
                var data = sr.ReadToEnd();
                lines = data.Replace("\r", string.Empty).Split('\n');
            }

            return base.Load();
        }
    }
}