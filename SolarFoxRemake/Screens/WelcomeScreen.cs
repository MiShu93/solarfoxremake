﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using MonoGame.Extended.BitmapFonts;
using SolarFoxRemake.Input;
using SolarFoxRemake.ScreenManagement;

namespace SolarFoxRemake.Screens
{
    public class WelcomeScreen : Screen
    {
        private const double BLINK_TIME = 0.5;
        private bool isShowTime = true;
        private double currentTime;

        private string text = "INSERT COIN";
        private BitmapFont font;
        private Vector2 position;

        public WelcomeScreen(ScreenManager screenManager) : base(screenManager)
        {
        }

        public override void LoadContent()
        {
            font = contentManager.Load<BitmapFont>("Fonts/comic_sans_32");

            var size = font.MeasureString(text);
            position = new Vector2
            {
                X = screenManager.DisplayCenter.X - size.X / 2f,
                Y = screenManager.DisplayCenter.Y - size.Y / 2f
            };
        }

        public override void HandleInput()
        {
            if (input.IsKeyPressed(Keys.Escape) ||
                input.IsKeyPressed(Keys.Back))
            {
                screenManager.RemoveRequest(GetType());
            }

            if (input.IsMouseButtonPressed(MouseButton.Left) ||
                input.IsGestureAvailable(GestureType.Tap) ||
                input.IsKeyPressed(Keys.Space) ||
                input.IsKeyPressed(Keys.Enter))
            {
                screenManager.RemoveRequest(GetType());
                screenManager.AddRequest(typeof(GameplayScreen));
            }
        }

        public override void Update(double dt)
        {
            currentTime += dt;
            if (currentTime >= BLINK_TIME)
            {
                isShowTime = !isShowTime;
                currentTime = 0;
            }
        }

        public override void Draw()
        {
            if (isShowTime)
            {
                spriteBatch.Begin(transformMatrix: screenManager.DefaultViewport.GetScaleMatrix());
                spriteBatch.DrawString(font, text, position, Color.White);
                spriteBatch.End();
            }
        }
    }
}