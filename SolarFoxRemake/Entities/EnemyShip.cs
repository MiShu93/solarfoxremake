﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SolarFoxRemake.Engine;
using System;

namespace SolarFoxRemake.Entities
{
    public class EnemyShip : Entity
    {
        private Random random;
        private const int CHANCE = 10;

        public bool IsGunReady { get; set; }
        public Vector2 GunPosition { get; private set; }
        public Vector2 GunDirection { get; private set; }
        private Point shotSize;
        private EnemyShipType type;

        public EnemyShip(Board board, Texture2D texture, Vector2 position, Random random, EnemyShipType type, Point shotSize) : base(board, texture, position)
        {
            this.random = random;
            this.shotSize = shotSize;
            this.type = type;

            Velocity = new Vector2(2);

            switch (type)
            {
                case EnemyShipType.Top:
                    Direction = Directions.Right;
                    GunDirection = Directions.Down;
                    break;
                case EnemyShipType.Bottom:
                    Direction = Directions.Left;
                    Rotation = MathHelper.ToRadians(180);
                    GunDirection = Directions.Up;
                    break;
            }

            UpdateGunPosition();
        }

        public void UpdateGunPosition()
        {
            switch (type)
            {
                case EnemyShipType.Top:
                    GunPosition = new Vector2(Position.X, Position.Y + origin.Y + shotSize.Y / 2f);
                    break;
                case EnemyShipType.Bottom:
                    GunPosition = new Vector2(Position.X, Position.Y - origin.Y - shotSize.Y / 2f);
                    break;
            }
        }

        public override void Update(double dt)
        {
            var row = (int)(Position.Y / board.CellSide);

            if (board.Contains(Position))
            {
                if (random.Next(100) % CHANCE == 0)
                {
                    IsGunReady = true;
                    UpdateGunPosition();
                }
            }

            base.Update(dt);

            if (Position.X < board.Centers[row, 1].X)
            {
                Direction = -Direction;
                Position = board.Centers[row, 1];
            }
            if (Position.X > board.Centers[row, Board.COLS - 2].X)
            {
                Direction = -Direction;
                Position = board.Centers[row, Board.COLS - 2];
            }
        }
    }

    public enum EnemyShipType
    {
        Top,
        Bottom
    }
}