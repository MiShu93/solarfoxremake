﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolarFoxRemake.Engine
{
    public enum Textures
    {
        Background,
        BluePoint,
        YellowPoint,
        RedPoint,
        PlayerShip,
        EnemyShip,
        GreenLaser
    }
}