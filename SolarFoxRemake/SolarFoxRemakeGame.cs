﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SolarFoxRemake.Engine;
using SolarFoxRemake.Factories;
using SolarFoxRemake.ScreenManagement;
using SolarFoxRemake.Screens;

namespace SolarFoxRemake
{
    public class SolarFoxRemakeGame : Game
    {
        private GraphicsDeviceManager graphicsManager;
        private ScreenManager screenManager;
        private ScreenFactory screenFactory;

        public static int VIRTUAL_HEIGHT { get; private set; }
        public static int VIRTUAL_WIDTH { get; private set; }

        public SolarFoxRemakeGame(LevelLoader levelLoader)
        {
            Content.RootDirectory = "Content";
            Services.AddService<LevelLoader>(levelLoader);

            graphicsManager = new GraphicsDeviceManager(this);
            graphicsManager.IsFullScreen = true;

            screenFactory = new ScreenFactory();
            Services.AddService<ScreenFactory>(screenFactory);

            screenManager = new ScreenManager(this);
            screenManager.AddRequest(typeof(WelcomeScreen));
            Components.Add(screenManager);
        }

        protected override void Initialize()
        {
            IsMouseVisible = true;

            var displayMode = graphicsManager.GraphicsDevice.DisplayMode;
            VIRTUAL_HEIGHT = 480;
            VIRTUAL_WIDTH = (int)(displayMode.Width / (displayMode.Height / (double)VIRTUAL_HEIGHT));

#if LINUX
            graphicsManager.PreferredBackBufferHeight = VIRTUAL_HEIGHT;
            graphicsManager.PreferredBackBufferWidth = VIRTUAL_WIDTH;
#else
            graphicsManager.PreferredBackBufferHeight = displayMode.Height;
            graphicsManager.PreferredBackBufferWidth = displayMode.Width;
#endif

            base.Initialize();
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            base.Draw(gameTime);
        }
    }
}