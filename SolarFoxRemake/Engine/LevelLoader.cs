﻿using Microsoft.Xna.Framework;
using System;

namespace SolarFoxRemake.Engine
{
    public abstract class LevelLoader
    {
        public Board Board { get; set; }

        public int currentLevel = 0;
        private int levelCount = 3;

        public bool IsEnd
        {
            get
            {
                return currentLevel == levelCount;
            }
        }

        protected string[] lines;

        protected string Filename
        {
            get
            {
                return string.Format("Content/Levels/lvl_{0}.txt", currentLevel);
            }
        }

        public virtual Level Load()
        {
            var level = new Level();

            for (int i = 0; i < lines.Length; i++)
            {
                for (int j = 0; j < lines[i].Length; j++)
                {
                    var ch = lines[i][j].ToString();
                    switch (ch)
                    {
                        case "1":
                        case "2":
                        case "3":
                            var strength = Convert.ToInt32(ch);
                            var point = new Tuple<int, Vector2>(strength, Board.Centers[i, j]);
                            level.PointPositions.Add(point);
                            break;

                        case "P":
                            level.PlayerPosition = Board.Centers[i, j];
                            break;
                    }
                }
            }

            ++currentLevel;

            return level;
        }
    }
}