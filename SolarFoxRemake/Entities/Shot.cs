﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SolarFoxRemake.Engine;

namespace SolarFoxRemake.Entities
{
    public class Shot : Entity
    {
        public Shot(Board board, Texture2D texture, Vector2 position, Vector2 direction) : base(board, texture, position)
        {
            Velocity = new Vector2(2);
            Direction = direction;
            if (direction == Directions.Up)
                Rotation = MathHelper.ToRadians(180);
        }
    }
}