﻿using Microsoft.Xna.Framework;

namespace SolarFoxRemake.Engine
{
    public class Board
    {
        public const int ROWS = 10;
        public const int COLS = 10;

        public Rectangle Bounds { get; private set; }
        public Vector2[,] Centers { get; private set; }
        public int CellSide { get; private set; }

        public Board()
        {
            CellSide = SolarFoxRemakeGame.VIRTUAL_HEIGHT / ROWS;

            var boundsWidth = CellSide * COLS;
            var boundsX1 = (SolarFoxRemakeGame.VIRTUAL_WIDTH - boundsWidth) / 2;

            Bounds = new Rectangle(boundsX1, 0, boundsX1 + boundsWidth, SolarFoxRemakeGame.VIRTUAL_HEIGHT);

            Centers = new Vector2[ROWS, COLS];
            for (int i = 0; i < ROWS; i++)
            {
                for (int j = 0; j < COLS; j++)
                {
                    int x = j * CellSide + CellSide / 2 + Bounds.X;
                    int y = i * CellSide + CellSide / 2;
                    Centers[i, j] = new Vector2(x, y);
                }
            }
        }

        public bool Contains(Vector2 position)
        {
            for (int i = 0; i < ROWS; i++)
                for (int j = 0; j < COLS; j++)
                    if (position == Centers[i, j])
                        return true;

            return false;
        }
    }
}