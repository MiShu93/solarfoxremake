﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SolarFoxRemake.Engine;
using System.Collections.Generic;

namespace SolarFoxRemake.Entities
{
    public class PointBox : Entity
    {
        private List<Texture2D> textures;
        private int strength;

        public PointBox(Board board, List<Texture2D> textures, Vector2 position, int strength) : base(board, textures[strength - 1], position)
        {
            this.textures = textures;
            this.strength = strength - 1;
        }

        public override void Update(double dt)
        {
            // Do nothing
        }

        public void Collect()
        {
            if (--strength < 0)
                IsDead = true;
            else
                texture = textures[strength];
        }
    }
}