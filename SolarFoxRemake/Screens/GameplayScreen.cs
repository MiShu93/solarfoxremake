﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Sprites;
using SolarFoxRemake.Engine;
using SolarFoxRemake.Entities;
using SolarFoxRemake.ScreenManagement;
using System;
using System.Collections.Generic;

namespace SolarFoxRemake.Screens
{
    public class GameplayScreen : Screen
    {
        private Sprite background;

        private Board board = new Board();
        private Random random = new Random();
        private Dictionary<Textures, Texture2D> textures = new Dictionary<Textures, Texture2D>();

        private PlayerShip player;
        private List<Entity> entities = new List<Entity>();
        private List<Shot> shots = new List<Shot>();
        private List<EnemyShip> enemies = new List<EnemyShip>();

        private double playTime;
        private const double START_DALEY = 1.5;
        private bool isPaused = true;
        private LevelLoader levelLoader;
        private Level currentLevel;

        public GameplayScreen(ScreenManager screenManager) : base(screenManager)
        {
            levelLoader = screenManager.Game.Services.GetService<LevelLoader>();
            levelLoader.Board = board;
        }

        public override void LoadContent()
        {
            textures[Textures.Background] = contentManager.Load<Texture2D>("Images/background");
            textures[Textures.PlayerShip] = contentManager.Load<Texture2D>("Images/player_ship");
            textures[Textures.EnemyShip] = contentManager.Load<Texture2D>("Images/enemy_ship");
            textures[Textures.BluePoint] = contentManager.Load<Texture2D>("Images/blue_point");
            textures[Textures.YellowPoint] = contentManager.Load<Texture2D>("Images/yellow_point");
            textures[Textures.RedPoint] = contentManager.Load<Texture2D>("Images/red_point");
            textures[Textures.GreenLaser] = contentManager.Load<Texture2D>("Images/green_laser");

            background = new Sprite(textures[Textures.Background]);
            background.Origin = textures[Textures.Background].Bounds.Size.ToVector2() / 2f;

            LoadLevel();
        }

        private void LoadLevel()
        {
            playTime = 0;
            isPaused = true;

            enemies.Clear();
            entities.Clear();
            shots.Clear();

            currentLevel = levelLoader.Load();

            foreach (var pointPosition in currentLevel.PointPositions)
            {
                entities.Add(new PointBox(
                    board,
                    new List<Texture2D> { textures[Textures.BluePoint], textures[Textures.YellowPoint], textures[Textures.RedPoint] },
                    pointPosition.Item2,
                    pointPosition.Item1
                    ));
            }

            player = new PlayerShip(board, textures[Textures.PlayerShip], currentLevel.PlayerPosition);
            entities.Add(player);

            enemies.Add(new EnemyShip(board, textures[Textures.EnemyShip], board.Centers[0, 1], random, EnemyShipType.Top, textures[Textures.GreenLaser].Bounds.Size));
            enemies.Add(new EnemyShip(board, textures[Textures.EnemyShip], board.Centers[Board.ROWS - 1, Board.COLS - 2], random, EnemyShipType.Bottom, textures[Textures.GreenLaser].Bounds.Size));
            entities.AddRange(enemies);
        }

        public override void HandleInput()
        {
            if (input.IsKeyPressed(Keys.Escape) ||
                input.IsKeyPressed(Keys.Back))
            {
                screenManager.RemoveRequest(GetType());
                screenManager.AddRequest(typeof(WelcomeScreen));
            }

            if (!isPaused)
                player.HandleInput(input);
        }

        public override void Update(double dt)
        {
            if (isPaused)
            {
                playTime += dt;
                if (playTime >= START_DALEY)
                    isPaused = false;
            }
            else
            {
                foreach (var enemy in enemies)
                {
                    if (enemy.IsGunReady)
                    {
                        var shot = new Shot(board, textures[Textures.GreenLaser], enemy.GunPosition, enemy.GunDirection);
                        entities.Add(shot);
                        enemy.IsGunReady = false;
                    }
                }

                foreach (var entity in entities)
                    entity.Update(dt);

                CheckCollisions();

                if (player.IsDead)
                {
                    screenManager.RemoveRequest(GetType());
                    screenManager.AddRequest(typeof(WelcomeScreen));
                    levelLoader.currentLevel = 0;
                }

                var result = true;
                foreach (var e in entities)
                    if (e is PointBox)
                        result = false;

                if (result)
                    if (!levelLoader.IsEnd)
                    {
                        LoadLevel();
                    }
                    else
                    {
                        screenManager.RemoveRequest(GetType());
                        screenManager.AddRequest(typeof(WelcomeScreen));
                        levelLoader.currentLevel = 0;
                    }
            }
        }

        public void CheckCollisions()
        {
            foreach (var entity in entities)
            {
                if (entity is Shot)
                {
                    if (player.CollisionBounds.Intersects(entity.CollisionBounds))
                        player.IsDead = true;

                    if (entity.Position.Y < board.Centers[0, 0].Y ||
                        entity.Position.Y > board.Centers[Board.ROWS - 1, 0].Y)
                    {
                        entity.IsDead = true;
                    }
                }

                if (entity is PointBox)
                {
                    if (player.Position == entity.Position)
                        (entity as PointBox).Collect();
                }
            }
            entities.RemoveAll(x => x.IsDead);
        }

        public override void Draw()
        {
            spriteBatch.Begin(transformMatrix: screenManager.DefaultViewport.GetScaleMatrix());

            spriteBatch.Draw(background);

            foreach (var entity in entities)
                entity.Draw(spriteBatch);

            spriteBatch.End();
        }
    }
}