﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace SolarFoxRemake.Input
{
    public class InputState
    {
        private KeyboardState currentKeyboardState;
        private KeyboardState previousKeyboardState;

        private MouseState currentMouseState;
        private MouseState previousMouseState;

        private TouchCollection currentTouchState;
        private TouchCollection previousTouchState;

        private List<GestureSample> gestures = new List<GestureSample>();
        public ReadOnlyCollection<GestureSample> Gestures { get { return gestures.AsReadOnly(); } }

        public InputState()
        {
            TouchPanel.EnabledGestures = GestureType.Tap;
        }

        public void Update()
        {
            previousKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();

            previousMouseState = currentMouseState;
            currentMouseState = Mouse.GetState();

            previousTouchState = currentTouchState;
            currentTouchState = TouchPanel.GetState();

            gestures.Clear();
            while (TouchPanel.IsGestureAvailable)
                gestures.Add(TouchPanel.ReadGesture());
        }

        public bool IsKeyPressed(Keys key)
        {
            return currentKeyboardState.IsKeyDown(key) && previousKeyboardState.IsKeyUp(key);
        }

        public bool IsKeyReleased(Keys key)
        {
            return currentKeyboardState.IsKeyUp(key) && previousKeyboardState.IsKeyDown(key);
        }

        public bool IsKeyDown(Keys key)
        {
            return currentKeyboardState.IsKeyDown(key);
        }

        public bool IsKeyUp(Keys key)
        {
            return currentKeyboardState.IsKeyUp(key);
        }

        private ButtonState GetMouseButtonState(MouseButton mouseButton, MouseState mouseState)
        {
            switch (mouseButton)
            {
                case MouseButton.Left:
                    return mouseState.LeftButton;
                case MouseButton.Right:
                    return mouseState.RightButton;
                default:
                    throw new ArgumentException();
            }
        }

        public Vector2 GetMousePosistion()
        {
            return currentMouseState.Position.ToVector2();
        }

        public bool IsMouseButtonPressed(MouseButton button)
        {
            ButtonState previousButtonState = GetMouseButtonState(button, previousMouseState);
            ButtonState currentButtonState = GetMouseButtonState(button, currentMouseState);

            return currentButtonState == ButtonState.Pressed && previousButtonState == ButtonState.Released;
        }

        public bool IsMouseButtonReleased(MouseButton button)
        {
            ButtonState previousButtonState = GetMouseButtonState(button, previousMouseState);
            ButtonState currentButtonState = GetMouseButtonState(button, currentMouseState);

            return currentButtonState == ButtonState.Released && previousButtonState == ButtonState.Pressed;
        }

        public bool IsMouseButtonDown(MouseButton button)
        {
            ButtonState currentButtonState = GetMouseButtonState(button, currentMouseState);

            return currentButtonState == ButtonState.Pressed;
        }

        public bool IsMouseButtonUp(MouseButton button)
        {
            ButtonState currentButtonState = GetMouseButtonState(button, currentMouseState);

            return currentButtonState == ButtonState.Released;
        }

        public bool IsGestureAvailable(GestureType gestureType)
        {
            return gestures.Any(x => x.GestureType == gestureType);
        }

        public GestureSample GetFirstGesture(GestureType gestureType)
        {
            return gestures.First(x => x.GestureType == gestureType);
        }
    }
}