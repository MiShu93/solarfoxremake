﻿using SolarFoxRemake.ScreenManagement;
using System;

namespace SolarFoxRemake.Factories
{
    public class ScreenFactory
    {
        public Screen CreateScreen(ScreenManager screenManager, Type screenType)
        {
            return Activator.CreateInstance(screenType, screenManager) as Screen;
        }
    }
}