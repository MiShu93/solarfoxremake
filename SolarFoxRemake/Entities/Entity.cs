﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Shapes;
using SolarFoxRemake.Engine;

namespace SolarFoxRemake.Entities
{
    public abstract class Entity
    {
        protected Board board;
        protected Texture2D texture;
        protected Vector2 origin;

        public float Rotation { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Direction { get; set; }
        public Vector2 Velocity { get; set; }
        public bool IsDead { get; set; }
        public RectangleF CollisionBounds { get; set; }

        public Entity(Board board, Texture2D texture, Vector2 position)
        {
            this.board = board;
            this.texture = texture;

            origin = new Vector2(texture.Width / 2f, texture.Height / 2f);
            Position = position;
            CalculateCollisionBound();
        }

        private void CalculateCollisionBound()
        {
            CollisionBounds = new RectangleF
            {
                X = Position.X - origin.X,
                Y = Position.Y - origin.Y,
                Width = texture.Width,
                Height = texture.Height
            };
        }

        public virtual void Update(double dt)
        {
            Position += Velocity * Direction;
            CalculateCollisionBound();
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                texture: texture,
                position: Position,
                origin: origin,
                rotation: Rotation,
                color: Color.White
                );

#if DEBUG
            spriteBatch.DrawRectangle(
                rectangle: CollisionBounds,
                color: Color.Red
                );
#endif
        }
    }
}