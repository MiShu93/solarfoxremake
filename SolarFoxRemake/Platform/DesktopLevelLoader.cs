﻿using SolarFoxRemake.Engine;
using System.IO;

namespace SolarFoxRemake.Platform
{
    public class DesktopLevelLoader : LevelLoader
    {
        public override Level Load()
        {
            lines = File.ReadAllLines(Filename);

            return base.Load();
        }
    }
}