﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace SolarFoxRemake.Engine
{
    public class Level
    {
        public Vector2 PlayerPosition { get; set; }
        public List<Tuple<int, Vector2>> PointPositions { get; set; } = new List<Tuple<int, Vector2>>();
    }
}