﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using SolarFoxRemake.Input;

namespace SolarFoxRemake.ScreenManagement
{
    public abstract class Screen : IScreen
    {
        protected ScreenManager screenManager;
        protected InputState input;
        protected ContentManager contentManager;
        protected SpriteBatch spriteBatch;

        public Screen(ScreenManager screenManager)
        {
            this.screenManager = screenManager;
            this.input = screenManager.Input;
            this.contentManager = screenManager.ContentManager;
            this.spriteBatch = screenManager.SpriteBatch;
        }

        public virtual void LoadContent()
        {
        }

        public virtual void UnloadContent()
        {
        }

        public virtual void HandleInput()
        {
        }

        public virtual void Update(double dt)
        {
        }

        public virtual void Draw()
        {
        }
    }
}